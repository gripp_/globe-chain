# getting started w globe chain

author: gripp (marshall@glasseyeballs.com)

blockchains are weird. i have trouble wrapping my mind all the way around them. i understand
 parts, but the globe chain is still in development, so there are design decisions still being
  made. this document is to help a new contributor to the project get the codebase running. before
   you start this, make sure youve read the [contributor agreement](https://bitbucket.org/gripp_/globe-chain/src/master/CONTRIBUTORS.md) and [license](https://bitbucket.org/gripp_/globe-chain/src/master/LICENSE.md).

to get started with the codebase, i would install [intellij community](https://www.jetbrains.com
/idea/) edition. this is the ide i have been using; it is free and easy to set up; and the entire
 project should just import cleanly via the project-from-existing-sources workflow. stop here and
  get the project set up. [install the ide](https://www.jetbrains.com/idea/). [import the
   repository](https://www.jetbrains.com/help/idea/import-project-or-module-wizard.html). try [building the project](https://www.jetbrains.com/help/idea/compiling-applications.html#compile_module).

once everything is building, there are three entryways to the program:

## 1/ run the unit tests

start with this one. it will confirm that the census library is working / consistent. in your project window, navigate to census > src > test. right click that directory and click run tests OR ^⬆R is the key command. confirm that it ran **37 tests** (if it ran the tests cleanly but there are a different number, update this doc).

## 2/ run the cli

in your project window, navigate to census > src > main > kotlin > RunCLI.kt. this is the main script for the command-line census interface. it is old and clunky and on its way to deprecation. but there is not yet a stable web interface, so it remains the simplest way to manually test operations on the census. if you try to run RunCLI.kt right now (^R) it should fail. the script requires an argument: a filename for the census it is reading from / writing to. you can set that via the [edit configuration dialog](https://www.jetbrains.com/help/idea/run-debug-configuration.html). set program arguments to `census.dat`; that file name is protected from upload by .gitignore. try not to commit your census files.

## 3/ run the server

in your project window, navigate to server > src > main > kotlin > RunServer.kt. you should be able to right click that and run it OR run it with ^R. this will run the server. it will try to load a census from the same file as above (`census.dat`), as defined in server > src > main > resources > application.properties. if no `census.dat` file is found, it will try to create the init blocks at the command line, though that functionality will probably go away in the near future. running the server in this way should let you go to [http://localhost:49149](http://localhost:49149) in your web browser to load the current home page.

thats it! once youve got the server running, head over to the issues tracker to see whats next on the docket.
