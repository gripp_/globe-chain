FROM openjdk:slim as BUILD

COPY src/ /src
WORKDIR /src
RUN ./gradlew --no-daemon shadowJar

FROM openjdk:11.0.9.1-jre-buster

COPY --from=BUILD /src/build/libs/globe-0.1-ALPHA-all.jar /bin/runner/run.jar

WORKDIR /bin/runner

CMD ["java", "-jar", "run.jar"]
