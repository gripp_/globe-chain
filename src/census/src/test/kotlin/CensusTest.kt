package globe.census.test

import globe.census.*
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.security.InvalidParameterException

class CensusTest {
  private class DatumInfo(
    val org : String,
    val issuer : Member,
    val issuerSignature : String,
    val conferee : Member,
    val role : String,
    val timestamp : Long,
    val revoke : Boolean = false)

  private val anotherOrg = "WAKANDA"
  private val anotherRole = "BLACKPANTHER"
  private val thirdOrg = "ASGARD"

  private val nakiaName = "nakia"
  private val okoyeName = "okoye"
  private val tchallaName = "tchalla"

  @Test
  fun `Newly created Census has correct start data`() {
    val census = makeCensus().first

    assert(census.size == 2)

    val memberDatum = census.datum(0)
    val memberCert = memberDatum.cert
    val zero = 0
    assert(memberDatum.previousHash == "0")
    assert(memberDatum.index == zero.toLong())
    assert(memberCert.issuer.name == tchallaName)
    assert(memberCert.conferee.name == tchallaName)
    assert(memberCert.role == Census.DEFAULTS.memberRole)
    assert(!memberCert.revoke)

    val issuerDatum = census.datum(1)
    val issuerCert = issuerDatum.cert
    val one = 1
    assert(issuerDatum.previousHash == memberDatum.hash)
    assert(issuerDatum.index == one.toLong())
    assert(issuerCert.issuer.name == tchallaName)
    assert(issuerCert.conferee.name == tchallaName)
    assert(issuerCert.role == Census.DEFAULTS.issuerRole)
    assert(!issuerCert.revoke)
  }

  @Test
  fun `New globe MEMBERs and ISSUERs can be added by globe ISSUERs`() {
    val censusInfo = makeCensus()
    val census = censusInfo.first
    val tchallaSignature = censusInfo.second

    val tchalla = census.datum(0).cert.issuer
    val nakia = MemberFactory.make(nakiaName)
    val okoye = MemberFactory.make(okoyeName).first

    for (datum in listOf(
      DatumInfo(
        Census.DEFAULTS.org,
        tchalla,
        tchallaSignature,
        nakia.first,
        Census.DEFAULTS.memberRole,
        System.currentTimeMillis()),
      DatumInfo(
        Census.DEFAULTS.org,
        tchalla,
        tchallaSignature,
        nakia.first,
        Census.DEFAULTS.issuerRole,
        System.currentTimeMillis()),
      DatumInfo(
        Census.DEFAULTS.org,
        nakia.first,
        nakia.second,
        okoye,
        Census.DEFAULTS.memberRole,
        System.currentTimeMillis()),
      DatumInfo(
        Census.DEFAULTS.org,
        nakia.first,
        nakia.second,
        okoye,
        Census.DEFAULTS.issuerRole,
        System.currentTimeMillis()))) {
      assertDatumAccepted(datum, census)
    }
  }

  @Test
  fun `No new globe roles may be added`() {
    val censusInfo = makeCensus()
    val census = censusInfo.first
    val tchalla = census.datum(0).cert.issuer
    val tchallaSignature = censusInfo.second

    val size = census.size
    assert(size == 2)

    val cert = CensusCertificate(
      Census.DEFAULTS.org,
      tchalla,
      MemberFactory.make(nakiaName).first,
      anotherRole,
      System.currentTimeMillis())

    census.addCertificate(
      cert, Encryptor.makePenStroke(cert, tchalla, tchallaSignature))

    assert(census.size == 2)
  }

  @Test
  fun `ISSUER for new org may be issued by globe ISSUER`() {
    val censusInfo = makeCensus()
    val census = censusInfo.first
    val tchallaSignature = censusInfo.second

    val tchalla = census.datum(0).cert.issuer
    val nakia = MemberFactory.make(nakiaName).first

    for (datum in listOf(
      DatumInfo(
        Census.DEFAULTS.org,
        tchalla,
        tchallaSignature,
        nakia,
        Census.DEFAULTS.memberRole,
        System.currentTimeMillis()),
      DatumInfo(
        anotherOrg,
        tchalla,
        tchallaSignature,
        nakia,
        Census.DEFAULTS.issuerRole,
        System.currentTimeMillis()))) {
      assertDatumAccepted(datum, census)
    }
  }

  @Test
  fun `ISSUER for new org may not be issued by ISSUER of other orgs`() {
    val censusInfo = makeCensus()
    val census = censusInfo.first
    val tchallaSignature = censusInfo.second

    val tchalla = census.datum(0).cert.issuer
    val nakia = MemberFactory.make(nakiaName)
    val okoye = MemberFactory.make(okoyeName).first

    for (datum in listOf(
      DatumInfo(
        Census.DEFAULTS.org,
        tchalla,
        tchallaSignature,
        nakia.first,
        Census.DEFAULTS.memberRole,
        System.currentTimeMillis()),
      DatumInfo(
        anotherOrg,
        tchalla,
        tchallaSignature,
        nakia.first,
        Census.DEFAULTS.issuerRole,
        System.currentTimeMillis()))) {
      assertDatumAccepted(datum, census)
    }

    assertDatumRejected(
      DatumInfo(
        thirdOrg,
        nakia.first,
        nakia.second,
        okoye,
        Census.DEFAULTS.issuerRole,
        System.currentTimeMillis()), census)
  }

  @Test
  fun `ISSUER must be first role on new org`() {
    val censusInfo = makeCensus()
    val census = censusInfo.first
    val tchallaSignature = censusInfo.second

    val tchalla = census.datum(0).cert.issuer

    assertDatumRejected(
      DatumInfo(
        anotherOrg, tchalla, tchallaSignature, tchalla, anotherRole,
        System.currentTimeMillis()),
      census)
    assertDatumAccepted(
      DatumInfo(
        anotherOrg,
        tchalla,
        tchallaSignature,
        tchalla,
        Census.DEFAULTS.issuerRole,
        System.currentTimeMillis()), census)
  }

  @Test
  fun `Globe ISSUER may create new role on org with ISSUER`() {
    val censusInfo = makeCensus()
    val census = censusInfo.first
    val tchallaSignature = censusInfo.second

    val tchalla = census.datum(0).cert.issuer
    val nakia = MemberFactory.make(nakiaName).first

    for (datum in listOf(
      DatumInfo(
        Census.DEFAULTS.org,
        tchalla,
        tchallaSignature,
        nakia,
        Census.DEFAULTS.memberRole,
        System.currentTimeMillis()),
      DatumInfo(
        anotherOrg,
        tchalla,
        tchallaSignature,
        nakia,
        Census.DEFAULTS.issuerRole,
        System.currentTimeMillis()),
      DatumInfo(
        anotherOrg, tchalla, tchallaSignature, nakia, anotherRole,
        System.currentTimeMillis()))) {
      assertDatumAccepted(datum, census)
    }
  }

  @Test
  fun `ISSUER on org may not create new role`() {
    val censusInfo = makeCensus()
    val census = censusInfo.first
    val tchallaSignature = censusInfo.second

    val tchalla = census.datum(0).cert.issuer
    val nakia = MemberFactory.make(nakiaName)
    val okoye = MemberFactory.make(okoyeName).first

    for (datum in listOf(
      DatumInfo(
        Census.DEFAULTS.org,
        tchalla,
        tchallaSignature,
        nakia.first,
        Census.DEFAULTS.memberRole,
        System.currentTimeMillis()),
      DatumInfo(
        Census.DEFAULTS.org,
        tchalla,
        tchallaSignature,
        okoye,
        Census.DEFAULTS.memberRole,
        System.currentTimeMillis()),
      DatumInfo(
        anotherOrg,
        tchalla,
        tchallaSignature,
        nakia.first,
        Census.DEFAULTS.issuerRole,
        System.currentTimeMillis()))) {
      assertDatumAccepted(datum, census)
    }

    assertDatumRejected(
      DatumInfo(
        anotherOrg,
        nakia.first,
        nakia.second,
        nakia.first,
        anotherRole,
        System.currentTimeMillis()),
      census)
  }

  @Test
  fun `ISSUER on org may issue new ISSUERS`() {
    val censusInfo = makeCensus()
    val census = censusInfo.first

    val tchalla = census.datum(0).cert.issuer
    val tchallaSignature = censusInfo.second

    val nakia = MemberFactory.make(nakiaName)
    val okoye = MemberFactory.make(okoyeName)

    for (datum in listOf(
      DatumInfo(
        Census.DEFAULTS.org,
        tchalla,
        tchallaSignature,
        nakia.first, Census.DEFAULTS.memberRole,
        System.currentTimeMillis()),
      DatumInfo(
        Census.DEFAULTS.org,
        tchalla,
        tchallaSignature,
        okoye.first,
        Census.DEFAULTS.memberRole,
        System.currentTimeMillis()),
      DatumInfo(
        anotherOrg,
        tchalla,
        tchallaSignature,
        nakia.first,
        Census.DEFAULTS.issuerRole,
        System.currentTimeMillis()),
      DatumInfo(
        anotherOrg,
        nakia.first,
        nakia.second,
        okoye.first,
        Census.DEFAULTS.issuerRole,
        System.currentTimeMillis()))) {
      assertDatumAccepted(datum, census)
    }
  }

  @Test
  fun `ISSUER on org may issue new members of existing roles`() {
    val censusInfo = makeCensus()
    val census = censusInfo.first
    val tchallaSignature = censusInfo.second

    val tchalla = census.datum(0).cert.issuer
    val nakia = MemberFactory.make(nakiaName)
    val okoye = MemberFactory.make(okoyeName).first

    for (datum in listOf(
      DatumInfo(
        Census.DEFAULTS.org,
        tchalla,
        tchallaSignature,
        nakia.first,
        Census.DEFAULTS.memberRole,
        System.currentTimeMillis()),
      DatumInfo(
        Census.DEFAULTS.org,
        tchalla,
        tchallaSignature,
        okoye,
        Census.DEFAULTS.memberRole,
        System.currentTimeMillis()),
      DatumInfo(
        anotherOrg,
        tchalla,
        tchallaSignature,
        nakia.first,
        Census.DEFAULTS.issuerRole,
        System.currentTimeMillis()),
      DatumInfo(
        anotherOrg,
        tchalla,
        tchallaSignature,
        nakia.first,
        anotherRole,
        System.currentTimeMillis()),
      DatumInfo(
        anotherOrg, nakia.first, nakia.second, okoye, anotherRole,
        System.currentTimeMillis()))) {
      assertDatumAccepted(datum, census)
    }
  }

  @Test
  fun `Only new MEMBER certs may be issued to new names`() {
    var censusInfo = makeCensus()
    var census = censusInfo.first
    var tchallaSignature = censusInfo.second

    var tchalla = census.datum(0).cert.issuer
    var nakia = MemberFactory.make(nakiaName).first

    assertDatumAccepted(
      DatumInfo(
        Census.DEFAULTS.org,
        tchalla,
        tchallaSignature,
        nakia,
        Census.DEFAULTS.memberRole,
        System.currentTimeMillis()),
      census)

    censusInfo = makeCensus()
    census = censusInfo.first
    tchallaSignature = censusInfo.second

    tchalla = census.datum(0).cert.issuer
    nakia = MemberFactory.make(nakiaName).first

    assertDatumRejected(
      DatumInfo(
        Census.DEFAULTS.org,
        tchalla,
        tchallaSignature,
        nakia,
        Census.DEFAULTS.issuerRole,
        System.currentTimeMillis()),
      census)
  }

  @Test
  fun `MEMBER certs may be revoked by globe ISSUERs`() {
    val censusInfo = makeCensus()
    val census = censusInfo.first
    val tchallaSignature = censusInfo.second

    val tchalla = census.datum(0).cert.issuer
    val nakia = MemberFactory.make(nakiaName).first

    for (datum in listOf(
      DatumInfo(
        Census.DEFAULTS.org,
        tchalla,
        tchallaSignature,
        nakia,
        Census.DEFAULTS.memberRole,
        System.currentTimeMillis()),
      DatumInfo(
        Census.DEFAULTS.org,
        tchalla,
        tchallaSignature,
        nakia,
        Census.DEFAULTS.memberRole,
        System.currentTimeMillis(),
        revoke = true))) {
      assertDatumAccepted(datum, census)
    }
  }

  @Test
  fun `New globe MEMBER certs may be issued to revoked names`() {
    val censusInfo = makeCensus()
    val census = censusInfo.first
    val tchallaSignature = censusInfo.second

    val tchalla = census.datum(0).cert.issuer
    val nakia = MemberFactory.make(nakiaName).first
    val nakia2 = MemberFactory.make(nakiaName).first

    for (datum in listOf(
      DatumInfo(
        Census.DEFAULTS.org,
        tchalla,
        tchallaSignature,
        nakia,
        Census.DEFAULTS.memberRole,
        System.currentTimeMillis()),
      DatumInfo(
        Census.DEFAULTS.org,
        tchalla,
        tchallaSignature,
        nakia,
        Census.DEFAULTS.memberRole,
        System.currentTimeMillis(),
        revoke = true),
      DatumInfo(
        Census.DEFAULTS.org,
        tchalla,
        tchallaSignature,
        nakia2,
        Census.DEFAULTS.memberRole,
        System.currentTimeMillis()))) {
      assertDatumAccepted(datum, census)
    }
  }

  @Test
  fun `Globe MEMBER certs may not be unrevoked`() {
    val censusInfo = makeCensus()
    val census = censusInfo.first
    val tchallaSignature = censusInfo.second

    val tchalla = census.datum(0).cert.issuer
    val nakia = MemberFactory.make(nakiaName).first

    for (datum in listOf(
      DatumInfo(
        Census.DEFAULTS.org,
        tchalla,
        tchallaSignature,
        nakia,
        Census.DEFAULTS.memberRole,
        System.currentTimeMillis()),
      DatumInfo(
        Census.DEFAULTS.org,
        tchalla,
        tchallaSignature,
        nakia,
        Census.DEFAULTS.memberRole,
        System.currentTimeMillis(),
        revoke = true))) {
      assertDatumAccepted(datum, census)
    }

    assertDatumRejected(
      DatumInfo(
        Census.DEFAULTS.org,
        tchalla,
        tchallaSignature,
        nakia,
        Census.DEFAULTS.memberRole,
        System.currentTimeMillis()),
      census)
  }

  @Test
  fun `Two globe MEMBERS may not be active with the same name`() {
    val censusInfo = makeCensus()
    val census = censusInfo.first
    val tchallaSignature = censusInfo.second

    val tchalla = census.datum(0).cert.issuer
    val nakia = MemberFactory.make(nakiaName).first
    val nakia2 = MemberFactory.make(nakiaName).first

    assertDatumAccepted(
      DatumInfo(
        Census.DEFAULTS.org,
        tchalla,
        tchallaSignature,
        nakia,
        Census.DEFAULTS.memberRole,
        System.currentTimeMillis()),
      census)
    assertDatumRejected(
      DatumInfo(
        Census.DEFAULTS.org,
        tchalla,
        tchallaSignature,
        nakia2,
        Census.DEFAULTS.memberRole,
        System.currentTimeMillis()),
      census)
  }

  @Test
  fun `Org names may not contain numbers`() {
    val censusInfo = makeCensus()
    val census = censusInfo.first
    val tchallaSignature = censusInfo.second

    val tchalla = census.datum(0).cert.issuer
    val nakia = MemberFactory.make(nakiaName).first

    assertDatumAccepted(
      DatumInfo(
        Census.DEFAULTS.org,
        tchalla,
        tchallaSignature,
        nakia,
        Census.DEFAULTS.memberRole,
        System.currentTimeMillis()),
      census)

    for (num in 0 until 9) {
      assertThrows<InvalidParameterException> {
        val cert = CensusCertificate(
          anotherOrg + num.toString(),
          tchalla,
          nakia,
          Census.DEFAULTS.issuerRole,
          System.currentTimeMillis())
        census.addCertificate(
          cert,
          Encryptor.makePenStroke(
            cert, tchalla, tchallaSignature))
      }
    }
  }

  @Test
  fun `Org names must be at least five characters long`() {
    val censusInfo = makeCensus()
    val census = censusInfo.first
    val tchallaSignature = censusInfo.second

    val tchalla = census.datum(0).cert.issuer
    val nakia = MemberFactory.make(nakiaName).first

    assertDatumAccepted(
      DatumInfo(
        Census.DEFAULTS.org,
        tchalla,
        tchallaSignature,
        nakia,
        Census.DEFAULTS.memberRole,
        System.currentTimeMillis()),
      census)

    assertThrows<InvalidParameterException> {
      val cert = CensusCertificate(
        anotherOrg.substring(0, 4),
        tchalla,
        nakia,
        Census.DEFAULTS.issuerRole,
        System.currentTimeMillis())
      census.addCertificate(
        cert, Encryptor.makePenStroke(cert, tchalla, tchallaSignature))
    }

    assertDatumAccepted(
      DatumInfo(
        anotherOrg.substring(0, 5),
        tchalla,
        tchallaSignature,
        nakia,
        Census.DEFAULTS.issuerRole,
        System.currentTimeMillis()),
      census)
  }

  @Test
  fun `Org names may be up to twenty characters long`() {
    val censusInfo = makeCensus()
    val census = censusInfo.first
    val tchallaSignature = censusInfo.second

    val tchalla = census.datum(0).cert.issuer
    val nakia = MemberFactory.make(nakiaName).first

    val longOrgName = anotherOrg + anotherOrg + anotherOrg

    assertDatumAccepted(
      DatumInfo(
        Census.DEFAULTS.org,
        tchalla,
        tchallaSignature,
        nakia,
        Census.DEFAULTS.memberRole,
        System.currentTimeMillis()),
      census)

    assertThrows<InvalidParameterException> {
      val cert = CensusCertificate(
        longOrgName.substring(0, 21),
        tchalla,
        nakia,
        Census.DEFAULTS.issuerRole,
        System.currentTimeMillis())
      census.addCertificate(
        cert, Encryptor.makePenStroke(cert, tchalla, tchallaSignature))
    }

    assertDatumAccepted(
      DatumInfo(
        longOrgName.substring(0, 20),
        tchalla,
        tchallaSignature,
        nakia,
        Census.DEFAULTS.issuerRole,
        System.currentTimeMillis()),
      census)
  }

  @Test
  fun `New certificates may not be farther than two hours in the future according to system time`() {
    val censusInfo = makeCensus()
    val localCensus = censusInfo.first
    val tchallaSignature = censusInfo.second

    val tchalla = localCensus.datum(0).cert.issuer
    val nakia = MemberFactory.make(nakiaName)

    val timestamps = listOf(
      System.currentTimeMillis(),
      System.currentTimeMillis() + (2.5 * 60 * 60 * 1000).toLong())

    for (datum in listOf(
      DatumInfo(
        Census.DEFAULTS.org,
        tchalla,
        tchallaSignature,
        nakia.first,
        Census.DEFAULTS.memberRole,
        timestamps[0]))) {
      assertDatumAccepted(datum, localCensus)
    }

    assert(localCensus.size == 3)

    for (datum in listOf(
      DatumInfo(
        Census.DEFAULTS.org,
        tchalla,
        tchallaSignature,
        nakia.first,
        Census.DEFAULTS.issuerRole,
        timestamps[1]))) {
      assertDatumRejected(datum, localCensus)
    }

    assert(localCensus.size == 3)
  }

  @Test
  fun `New certificates may not be older than the MPT of the last eleven blocks`() {
    val censusInfo = makeCensus()
    val census = censusInfo.first

    val tchalla = Pair(census.datum(0).cert.issuer, censusInfo.second)
    val nakia = MemberFactory.make(nakiaName)
    val okoye = MemberFactory.make(okoyeName)

    val timestamps = listOf(
      census.datum(0).cert.timestamp,
      census.datum(1).cert.timestamp,
      System.currentTimeMillis(),
      System.currentTimeMillis(),
      System.currentTimeMillis(),
      System.currentTimeMillis(),
      System.currentTimeMillis(),
      System.currentTimeMillis(),
      System.currentTimeMillis(),
      System.currentTimeMillis(),
      System.currentTimeMillis())

    val mpt = (timestamps.reduce { acc, l -> acc + l }) / 11

    for (datum in listOf(
      DatumInfo(
        Census.DEFAULTS.org,
        tchalla.first,
        tchalla.second,
        nakia.first,
        Census.DEFAULTS.memberRole,
        timestamps[2]),
      DatumInfo(
        Census.DEFAULTS.org,
        tchalla.first,
        tchalla.second,
        nakia.first,
        Census.DEFAULTS.issuerRole,
        timestamps[3]),
      DatumInfo(
        anotherOrg,
        tchalla.first,
        tchalla.second,
        nakia.first,
        Census.DEFAULTS.issuerRole,
        timestamps[4]),
      DatumInfo(
        anotherOrg,
        tchalla.first,
        tchalla.second,
        nakia.first,
        Census.DEFAULTS.memberRole,
        timestamps[5]),
      DatumInfo(
        Census.DEFAULTS.org,
        tchalla.first,
        tchalla.second,
        okoye.first,
        Census.DEFAULTS.memberRole,
        timestamps[6]),
      DatumInfo(
        anotherOrg,
        tchalla.first,
        tchalla.second,
        nakia.first,
        anotherRole,
        timestamps[7]),
      DatumInfo(
        anotherOrg,
        nakia.first,
        nakia.second,
        okoye.first,
        Census.DEFAULTS.memberRole,
        timestamps[8]),
      DatumInfo(
        anotherOrg,
        nakia.first,
        nakia.second,
        okoye.first,
        anotherRole,
        timestamps[9]),
      DatumInfo(
        anotherOrg,
        nakia.first,
        nakia.second,
        okoye.first,
        anotherRole,
        timestamps[10],
        revoke = true),
    )) {
      assertDatumAccepted(datum, census)
    }

    assertDatumRejected(
      DatumInfo(
        thirdOrg,
        tchalla.first,
        tchalla.second,
        nakia.first,
        Census.DEFAULTS.issuerRole,
        mpt),
      census)
  }

  @Test
  fun `Duplicate certs are discarded`() {
    val censusInfo = makeCensus()
    val localCensus = censusInfo.first
    val tchallaSignature = censusInfo.second

    val tchalla = localCensus.datum(0).cert.issuer
    val nakia = MemberFactory.make(nakiaName)

    val timestamps = listOf(
      System.currentTimeMillis(),
      System.currentTimeMillis(),
      System.currentTimeMillis())

    for (datum in listOf(
      DatumInfo(
        Census.DEFAULTS.org,
        tchalla,
        tchallaSignature,
        nakia.first,
        Census.DEFAULTS.memberRole,
        timestamps[0]))) {
      assertDatumAccepted(datum, localCensus)
    }

    val networkCensus = Census(
      Json.decodeFromString(
        String(localCensus.toByteArray())))

    assert(localCensus.size == 3)
    assert(networkCensus.size == 3)

    for (datum in listOf(
      DatumInfo(
        Census.DEFAULTS.org,
        tchalla,
        tchallaSignature,
        nakia.first,
        Census.DEFAULTS.issuerRole,
        timestamps[1]))) {
      assertDatumAccepted(datum, localCensus)
    }

    for (datum in listOf(
      DatumInfo(
        Census.DEFAULTS.org,
        tchalla,
        tchallaSignature,
        nakia.first,
        Census.DEFAULTS.issuerRole,
        timestamps[2]))) {
      assertDatumAccepted(datum, networkCensus)
    }

    for (datum : CensusDatum in Json.decodeFromString<List<CensusDatum>>(
      String(networkCensus.toByteArray()))) {
      localCensus.addCertificate(datum.cert, datum.penStroke)
    }

    assert(networkCensus.size == 4)
    assert(localCensus.size == 4)

    val ultimateDatum = localCensus.datum(3)
    val penultimateDatum = localCensus.datum(2)

    assert(ultimateDatum.index == (3).toLong())
    assert(penultimateDatum.index == (2).toLong())

    assert(ultimateDatum.previousHash == penultimateDatum.hash)

    assert(ultimateDatum.cert.timestamp == timestamps[1])
    assert(penultimateDatum.cert.timestamp == timestamps[0])

    assert(!ultimateDatum.cert.revoke)
    assert(ultimateDatum.cert.conferee == nakia.first)
    assert(ultimateDatum.cert.issuer == tchalla)
    assert(ultimateDatum.cert.org == Census.DEFAULTS.org)
    assert(ultimateDatum.cert.role == Census.DEFAULTS.issuerRole)

    assert(!penultimateDatum.cert.revoke)
    assert(penultimateDatum.cert.conferee == nakia.first)
    assert(penultimateDatum.cert.issuer == tchalla)
    assert(penultimateDatum.cert.org == Census.DEFAULTS.org)
    assert(penultimateDatum.cert.role == Census.DEFAULTS.memberRole)
  }

  @Test
  fun `Certs invalidated by backdating are returned as rejected`() {
    val censusInfo = makeCensus()
    val census = censusInfo.first
    val tchallaSignature = censusInfo.second

    val tchalla = census.datum(0).cert.issuer
    val nakia = MemberFactory.make(nakiaName)
    val okoye = MemberFactory.make(okoyeName)

    val stamp = System.currentTimeMillis()

    val timestamps = listOf(
      stamp, stamp + 10, stamp + 20, stamp + 30, stamp + 40, stamp + 50)

    println(timestamps)

    for (datum in listOf(
      DatumInfo(
        Census.DEFAULTS.org,
        tchalla,
        tchallaSignature,
        nakia.first,
        Census.DEFAULTS.memberRole,
        timestamps[0]),
      DatumInfo(
        anotherOrg,
        tchalla,
        tchallaSignature,
        nakia.first,
        Census.DEFAULTS.issuerRole,
        timestamps[1]),
      DatumInfo(
        anotherOrg,
        tchalla,
        tchallaSignature,
        nakia.first,
        Census.DEFAULTS.memberRole,
        timestamps[2]),
      DatumInfo(
        Census.DEFAULTS.org,
        tchalla,
        tchallaSignature,
        okoye.first,
        Census.DEFAULTS.memberRole,
        timestamps[3]),
      DatumInfo(
        anotherOrg,
        nakia.first,
        nakia.second,
        okoye.first,
        Census.DEFAULTS.memberRole,
        timestamps[5]))) {
      assertDatumAccepted(datum, census)
    }

    val cert = CensusCertificate(
      anotherOrg,
      nakia.first,
      okoye.first,
      Census.DEFAULTS.memberRole,
      timestamps[5],
      false)

    assertDatumAccepted(
      DatumInfo(
        anotherOrg,
        nakia.first,
        nakia.second,
        nakia.first,
        Census.DEFAULTS.issuerRole,
        timestamps[4],
        revoke = true),
      census,
      rejections = listOf(cert))
  }

  @Test
  fun `Can add remote data at the end of the ledger`() {
    val censusInfo = makeCensus()
    val localCensus = censusInfo.first
    val tchallaSignature = censusInfo.second

    val tchalla = localCensus.datum(0).cert.issuer
    val nakia = MemberFactory.make(nakiaName)
    val okoye = MemberFactory.make(okoyeName)

    val timestamps = listOf(
      System.currentTimeMillis(),
      System.currentTimeMillis(),
      System.currentTimeMillis(),
      System.currentTimeMillis(),
      System.currentTimeMillis(),
      System.currentTimeMillis())

    for (datum in listOf(
      DatumInfo(
        Census.DEFAULTS.org,
        tchalla,
        tchallaSignature,
        nakia.first,
        Census.DEFAULTS.memberRole,
        timestamps[0]),
      DatumInfo(
        Census.DEFAULTS.org,
        tchalla,
        tchallaSignature,
        nakia.first,
        Census.DEFAULTS.issuerRole,
        timestamps[1]),
      DatumInfo(
        Census.DEFAULTS.org,
        nakia.first,
        nakia.second,
        okoye.first,
        Census.DEFAULTS.memberRole,
        timestamps[2]),
      DatumInfo(
        Census.DEFAULTS.org,
        nakia.first,
        nakia.second,
        okoye.first,
        Census.DEFAULTS.issuerRole,
        timestamps[3]))) {
      assertDatumAccepted(datum, localCensus)
    }

    val networkCensus = Census(
      Json.decodeFromString(
        String(localCensus.toByteArray())))

    assert(localCensus.size == 6)
    assert(networkCensus.size == 6)

    for (datum in listOf(
      DatumInfo(
        anotherOrg,
        tchalla,
        tchallaSignature,
        nakia.first,
        Census.DEFAULTS.issuerRole,
        timestamps[4]),
      DatumInfo(
        anotherOrg,
        tchalla,
        tchallaSignature,
        nakia.first,
        anotherRole,
        timestamps[5]))) {
      assertDatumAccepted(datum, networkCensus)
    }

    for (datum : CensusDatum in Json.decodeFromString<List<CensusDatum>>(
      String(networkCensus.toByteArray()))) {
      localCensus.addCertificate(datum.cert, datum.penStroke)
    }

    assert(networkCensus.size == 8)
    assert(localCensus.size == 8)

    val ultimateDatum = localCensus.datum(7)
    val penultimateDatum = localCensus.datum(6)
    val semipenultimateDatum = localCensus.datum(5)
    val fourthToLast = localCensus.datum(4)

    assert(ultimateDatum.index == (7).toLong())
    assert(penultimateDatum.index == (6).toLong())
    assert(semipenultimateDatum.index == (5).toLong())
    assert(fourthToLast.index == (4).toLong())

    assert(ultimateDatum.previousHash == penultimateDatum.hash)
    assert(penultimateDatum.previousHash == semipenultimateDatum.hash)
    assert(semipenultimateDatum.previousHash == fourthToLast.hash)

    assert(ultimateDatum.cert.timestamp == timestamps[5])
    assert(penultimateDatum.cert.timestamp == timestamps[4])
    assert(semipenultimateDatum.cert.timestamp == timestamps[3])
    assert(fourthToLast.cert.timestamp == timestamps[2])

    assert(!ultimateDatum.cert.revoke)
    assert(ultimateDatum.cert.conferee == nakia.first)
    assert(ultimateDatum.cert.issuer == tchalla)
    assert(ultimateDatum.cert.org == anotherOrg)
    assert(ultimateDatum.cert.role == anotherRole)

    assert(!penultimateDatum.cert.revoke)
    assert(penultimateDatum.cert.conferee == nakia.first)
    assert(penultimateDatum.cert.issuer == tchalla)
    assert(penultimateDatum.cert.org == anotherOrg)
    assert(penultimateDatum.cert.role == Census.DEFAULTS.issuerRole)

    assert(!semipenultimateDatum.cert.revoke)
    assert(semipenultimateDatum.cert.conferee == okoye.first)
    assert(semipenultimateDatum.cert.issuer == nakia.first)
    assert(semipenultimateDatum.cert.org == Census.DEFAULTS.org)
    assert(semipenultimateDatum.cert.role == Census.DEFAULTS.issuerRole)

    assert(!fourthToLast.cert.revoke)
    assert(fourthToLast.cert.conferee == okoye.first)
    assert(fourthToLast.cert.issuer == nakia.first)
    assert(fourthToLast.cert.org == Census.DEFAULTS.org)
    assert(fourthToLast.cert.role == Census.DEFAULTS.memberRole)
  }

  @Test
  fun `Can add remote data in the middle of the ledger`() {
    val censusInfo = makeCensus()
    val localCensus = censusInfo.first
    val tchallaSignature = censusInfo.second

    val tchalla = localCensus.datum(0).cert.issuer
    val nakia = MemberFactory.make(nakiaName)
    val okoye = MemberFactory.make(okoyeName)

    for (datum in listOf(
      DatumInfo(
        Census.DEFAULTS.org,
        tchalla,
        tchallaSignature,
        nakia.first,
        Census.DEFAULTS.memberRole,
        System.currentTimeMillis()),
      DatumInfo(
        Census.DEFAULTS.org,
        tchalla,
        tchallaSignature,
        nakia.first,
        Census.DEFAULTS.issuerRole,
        System.currentTimeMillis()))) {
      assertDatumAccepted(datum, localCensus)
    }

    val networkCensus = Census(
      Json.decodeFromString(String(localCensus.toByteArray())))

    val fourthToLastTimestamp = System.currentTimeMillis()
    val semipenultimateTimestamp = System.currentTimeMillis()

    for (datum in listOf(
      DatumInfo(
        Census.DEFAULTS.org,
        nakia.first,
        nakia.second,
        okoye.first,
        Census.DEFAULTS.memberRole,
        fourthToLastTimestamp),
      DatumInfo(
        Census.DEFAULTS.org,
        nakia.first,
        nakia.second,
        okoye.first,
        Census.DEFAULTS.issuerRole,
        semipenultimateTimestamp))) {
      assertDatumAccepted(datum, networkCensus)
    }

    assert(localCensus.size == 4)
    assert(networkCensus.size == 6)

    val penultimateTimestamp = System.currentTimeMillis()
    val ultimateTimestamp = System.currentTimeMillis()

    for (datum in listOf(
      DatumInfo(
        anotherOrg,
        tchalla,
        tchallaSignature,
        nakia.first,
        Census.DEFAULTS.issuerRole,
        penultimateTimestamp),
      DatumInfo(
        anotherOrg,
        tchalla,
        tchallaSignature,
        nakia.first,
        anotherRole,
        ultimateTimestamp))) {
      assertDatumAccepted(datum, localCensus)
    }

    for (datum : CensusDatum in Json.decodeFromString<List<CensusDatum>>(
      String(networkCensus.toByteArray()))) {
      localCensus.addCertificate(datum.cert, datum.penStroke)
    }

    assert(localCensus.size == 8)

    val ultimateDatum = localCensus.datum(7)
    val penultimateDatum = localCensus.datum(6)
    val semipenultimateDatum = localCensus.datum(5)
    val fourthToLast = localCensus.datum(4)

    assert(ultimateDatum.index == (7).toLong())
    assert(penultimateDatum.index == (6).toLong())
    assert(semipenultimateDatum.index == (5).toLong())
    assert(fourthToLast.index == (4).toLong())

    assert(ultimateDatum.previousHash == penultimateDatum.hash)
    assert(penultimateDatum.previousHash == semipenultimateDatum.hash)
    assert(semipenultimateDatum.previousHash == fourthToLast.hash)

    assert(ultimateDatum.cert.timestamp == ultimateTimestamp)
    assert(penultimateDatum.cert.timestamp == penultimateTimestamp)
    assert(semipenultimateDatum.cert.timestamp == semipenultimateTimestamp)
    assert(fourthToLast.cert.timestamp == fourthToLastTimestamp)

    assert(!ultimateDatum.cert.revoke)
    assert(ultimateDatum.cert.conferee == nakia.first)
    assert(ultimateDatum.cert.issuer == tchalla)
    assert(ultimateDatum.cert.org == anotherOrg)
    assert(ultimateDatum.cert.role == anotherRole)

    assert(!penultimateDatum.cert.revoke)
    assert(penultimateDatum.cert.conferee == nakia.first)
    assert(penultimateDatum.cert.issuer == tchalla)
    assert(penultimateDatum.cert.org == anotherOrg)
    assert(penultimateDatum.cert.role == Census.DEFAULTS.issuerRole)

    assert(!semipenultimateDatum.cert.revoke)
    assert(semipenultimateDatum.cert.conferee == okoye.first)
    assert(semipenultimateDatum.cert.issuer == nakia.first)
    assert(semipenultimateDatum.cert.org == Census.DEFAULTS.org)
    assert(semipenultimateDatum.cert.role == Census.DEFAULTS.issuerRole)

    assert(!fourthToLast.cert.revoke)
    assert(fourthToLast.cert.conferee == okoye.first)
    assert(fourthToLast.cert.issuer == nakia.first)
    assert(fourthToLast.cert.org == Census.DEFAULTS.org)
    assert(fourthToLast.cert.role == Census.DEFAULTS.memberRole)
  }

  private fun assertDatumAccepted(
    datumInfo : DatumInfo,
    census : Census,
    rejections : List<CensusCertificate> = listOf()) {
    val size = census.size
    val cert = CensusCertificate(
      datumInfo.org,
      datumInfo.issuer,
      datumInfo.conferee,
      datumInfo.role,
      datumInfo.timestamp,
      datumInfo.revoke)
    val response = census.addCertificate(
      cert,
      Encryptor.makePenStroke(
        cert,
        datumInfo.issuer,
        datumInfo.issuerSignature))
    assert(response.first)

    for (rejectedCert in rejections) {
      assert(response.second.contains(rejectedCert))
    }

    assert(census.size == (size + 1 - response.second.size))

    validateData(census)
  }

  private fun assertDatumRejected(datumInfo : DatumInfo, census : Census) {
    val size = census.size
    val lastDatum = census.datum(size - 1)

    val cert = CensusCertificate(
      datumInfo.org,
      datumInfo.issuer,
      datumInfo.conferee,
      datumInfo.role,
      datumInfo.timestamp,
      datumInfo.revoke)
    val response = census.addCertificate(
      cert,
      Encryptor.makePenStroke(
        cert,
        datumInfo.issuer,
        datumInfo.issuerSignature))

    assert(!response.first)
    assert(response.second.contains(cert))

    assert(census.size == size)
    assert(lastDatum == census.datum(size - 1))

    validateData(census)
  }

  private fun makeCensus() : Pair<Census, String> {
    val tchallaInfo = MemberFactory.make(tchallaName)
    return Pair(
      Census(
        tchallaInfo.first,
        tchallaInfo.second),
      tchallaInfo.second)
  }

  private fun validateData(census : Census) {
    for (i in IntRange(2, census.size - 1)) {
      val currentDatum = census.datum(i)
      val previousDatum = census.datum(i - 1)

      assert(currentDatum.previousHash == previousDatum.hash)
      assert(currentDatum.index == i.toLong())
    }
  }
}