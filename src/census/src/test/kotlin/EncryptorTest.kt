package globe.census.test

import globe.census.Encryptor
import globe.census.MemberFactory
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.util.*
import javax.crypto.BadPaddingException

class EncryptorTest {
  val message = "meowwwwwwwwwwwwrrrrrrrr"
  val name = "nixon"
  val yesASignature = "napz@llDA7**********************"
  val notASignature = "foodisoverrated"
  val salt = "x/LqtQu9fE8="
  val publicKey = "f436L7COh7PfsSFM0RzDVmC+xySFHIEIw7m4Ovo2IUY="

  val invalidNameCharacters = "~!@#$%^&*()_+=-<>?,./';[]{}\\|"
  val lettersAndNumbersName1 = "abcdefghijklmno012"
  val lettersAndNumbersName2 = "pqrstuvwxyz3456789"

  @Test
  fun `Names may contain lowercase letters and digits`() {
    assert(Encryptor.validateName(lettersAndNumbersName1))
    assert(Encryptor.validateName(lettersAndNumbersName2))
  }

  @Test
  fun `Names may not contain special characters`() {
    for (character in invalidNameCharacters) {
      assert(!Encryptor.validateName(name + character))
    }
  }

  @Test
  fun `Names may not be shorter than five characters`() {
    assert(!Encryptor.validateName(lettersAndNumbersName1.substring(0, 4)))
    assert(Encryptor.validateName(lettersAndNumbersName1.substring(0, 5)))
  }

  @Test
  fun `Names may not be longer than twenty characters`() {
    val longName = lettersAndNumbersName1 + lettersAndNumbersName2
    assert(!Encryptor.validateName(longName.substring(0, 21)))
    assert(Encryptor.validateName(longName.substring(0, 20)))
  }

  @Test
  fun `Signatures must contain a lowercase letter`() {
    val signatureWithNoLowercase = "A1*****************************"
    assert(!Encryptor.validateSignature("$signatureWithNoLowercase*"))
    for (character in "abcdefghijklmnopqrstuvwxyz") {
      assert(Encryptor.validateSignature(signatureWithNoLowercase + character))
    }
  }

  @Test
  fun `Signatures must contain an uppercase letter`() {
    val signatureWithNoUppercase = "a1*****************************"
    assert(!Encryptor.validateSignature("{$signatureWithNoUppercase}a"))
    for (character in "ABCDEFGHIJKLMNOPQRSTUVWXYZ") {
      assert(Encryptor.validateSignature("$signatureWithNoUppercase$character"))
    }
  }

  @Test
  fun `Signatures must contain a digit`() {
    val signatureWithNoDigit = "aB*****************************"
    assert(!Encryptor.validateSignature(signatureWithNoDigit + "a"))
    for (character in "0123456789") {
      assert(Encryptor.validateSignature(signatureWithNoDigit + character))
    }
  }

  @Test
  fun `Signatures must contain a valid special character`() {
    val signatureWithNoSpecial = "aB00000000000000000000000000000"
    assert(!Encryptor.validateSignature(signatureWithNoSpecial + "a"))
    for (character in "~@#\$%^&*+|?/.,") {
      assert(Encryptor.validateSignature(signatureWithNoSpecial + character))
    }
  }

  @Test
  fun `Signatures may not contain an invalid special character`() {
    val otherwiseValidSignature = "aB*0000000000000000000000000000"
    assert(Encryptor.validateSignature(otherwiseValidSignature + "a"))
    for (character in "\\=_-{}[]()!`\"' \t") {
      assert(!Encryptor.validateSignature(otherwiseValidSignature + character))
    }
  }

  @Test
  fun `Signatures must be exactly 32 characters long`() {
    val tooShortSignature = "aB0****************************"
    val tooLongSignature = "aB0******************************"
    val justRightSignature = "aB0*****************************"
    assert(!Encryptor.validateSignature(tooShortSignature))
    assert(!Encryptor.validateSignature(tooLongSignature))
    assert(Encryptor.validateSignature(justRightSignature))
  }

  @Test
  fun `Messages can be encoded and decoded by a members signature`() {
    val memberInfo = MemberFactory.make(name)
    val member = memberInfo.first
    val signature = memberInfo.second
    val encryptedMessage = Encryptor.encrypt(message, member)

    assert(encryptedMessage != message)
    assert(
      Encryptor.decrypt(
        encryptedMessage, member.name, signature, member.salt) == message)
    assertThrows<BadPaddingException> {
      Encryptor.decrypt(
        encryptedMessage, member.name, notASignature, member.salt)
    }
  }

  @Test
  fun `Public key creation works`() {
    assert(Encryptor.makePublicKeyString(name, yesASignature, salt) == publicKey)
  }

  @Test
  fun `Salt size is consistent`() {
    val salt = Encryptor.makeSalt()
    val saltBytes = Base64.getDecoder().decode(salt)
    assert(saltBytes.size == 8)
  }
}