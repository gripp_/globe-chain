package globe.census.test

import org.junit.jupiter.api.Test

import globe.census.Member

class MemberTest {
  val name = "fakename"
  val publicKey = "fakepublickey"
  val salt = "fakesalt"

  @Test
  fun `Member equality is correct`() {
    val member = Member(name, publicKey, salt)
    val differentName = Member(name + "1", publicKey, salt)
    val differentKey = Member(name, publicKey + "2", salt)
    val differentSalt = Member(name, publicKey, salt + "3")
    val differentEverything = Member(name + "4", publicKey + "4", salt + "4")
    val notAMember = null
    val sameEverything = Member(name, publicKey, salt)

    assert(member != differentName)
    assert(member != differentKey)
    assert(member != differentSalt)
    assert(member != differentEverything)
    assert(member != notAMember)

    assert(member == sameEverything)
  }
}