package globe.census.test

import globe.census.CensusCertificate
import globe.census.Encryptor
import globe.census.MemberFactory
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.security.InvalidParameterException

class MemberFactoryTest {
  val message = "meow"
  val name = "nixon"

  @Test
  fun `New member name is correct`() {
    val member = MemberFactory.make(name).first
    assert(member.name == name)
  }

  @Test
  fun `New member public key-salt pair works`() {
    val memberInfo = MemberFactory.make(name)
    val secretMessage = Encryptor.encrypt(message, memberInfo.first)
    assert(
      Encryptor.decrypt(
        secretMessage,
        memberInfo.first.name,
        memberInfo.second,
        memberInfo.first.salt) == message)
  }

  @Test
  fun `Names may not contain uppercase letters`() {
    assertThrows<InvalidParameterException> {
      MemberFactory.make(name.toUpperCase()).first
    }
  }

  @Test
  fun `Member signatures can be opened with member public key`() {
    val memberInfo = MemberFactory.make(name)

    val cert = CensusCertificate(
      "DEFAULT",
      memberInfo.first,
      memberInfo.first,
      "SOMEROLE",
      System.currentTimeMillis())

    val penStroke = Encryptor.makePenStroke(
      cert, memberInfo.first, memberInfo.second)

    assert(Encryptor.testPenStroke(penStroke, cert))
  }
}