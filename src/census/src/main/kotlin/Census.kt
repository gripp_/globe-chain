package globe.census

import globe.census.comb.Comb
import globe.census.comb.teeth.types.Conferee
import globe.census.comb.teeth.types.ConfereeName
import globe.census.comb.teeth.types.Org
import globe.census.comb.teeth.types.Role
import globe.census.comb.types.MemberOrgRoleIsUnique
import globe.census.comb.types.OrgRoleIsUnique
import java.security.InvalidParameterException
import kotlin.math.max
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

class Census {
  object DEFAULTS {
    val org = "GLOBE"

    val issuerRole = "ISSUER"
    val memberRole = "MEMBER"
  }

  val size : Int
    get() = ledger.size

  val snapshot : List<CensusDatum>
    get() = ledger.toList()

  val mpt : Long
    get() {
      var mpt : Long = 0
      for (datum in ledger.subList(
          max(0, ledger.size - numBlocksInMPT),
          ledger.size)) {
        mpt += datum.cert.timestamp
      }
      return mpt / numBlocksInMPT
    }

  private var ledger = mutableListOf<CensusDatum>()
  private val maxFutureBlockTime = 2 * 60 * 60 * 1000
  private val numBlocksInMPT = 11

  constructor(firstMember : Member, signature : String) {
    val memberCert = CensusCertificate(
        DEFAULTS.org,
        firstMember,
        firstMember,
        DEFAULTS.memberRole,
        System.currentTimeMillis())

    val issuerCert = CensusCertificate(
        DEFAULTS.org,
        firstMember,
        firstMember,
        DEFAULTS.issuerRole,
        System.currentTimeMillis())

    ledger.add(
        CensusDatum(
            0,
            "0",
            memberCert,
            Encryptor.makePenStroke(memberCert, firstMember, signature)))
    ledger.add(
        CensusDatum(
            1,
            ledger.last().hash,
            issuerCert,
            Encryptor.makePenStroke(issuerCert, firstMember, signature)))
  }

  constructor(existingLedger : List<CensusDatum>) {
    if (existingLedger.size < 2) {
      throw InvalidParameterException("Ledger is too short to be valid.")
    }

    val invalidLedger = InvalidParameterException(
        "Ledger contains an invalid datum.")

    val firstDatum = existingLedger[0]
    val secondDatum = existingLedger[1]

    if (
        firstDatum.index != (0).toLong() ||
        firstDatum.previousHash != "0" ||
        firstDatum.cert.org != DEFAULTS.org ||
        firstDatum.cert.issuer != firstDatum.cert.conferee ||
        firstDatum.cert.role != DEFAULTS.memberRole ||
        secondDatum.index != (1).toLong() ||
        secondDatum.previousHash != firstDatum.hash ||
        secondDatum.cert.org != DEFAULTS.org ||
        secondDatum.cert.issuer != secondDatum.cert.conferee ||
        secondDatum.cert.issuer != firstDatum.cert.issuer ||
        secondDatum.cert.role != DEFAULTS.issuerRole) {
      throw invalidLedger
    }

    ledger.add(firstDatum)
    ledger.add(secondDatum)

    for ((index, datum) in existingLedger.withIndex()) {
      if (index == 0 || index == 1) {
        continue
      }

      if (
          !validateCertificate(datum.cert) ||
          !Encryptor.testPenStroke(datum.penStroke, datum.cert) ||
          !validateDatum(datum)) {
        throw invalidLedger
      }

      ledger.add(datum)
    }
  }

  fun addCertificate(
      nextCert : CensusCertificate,
      penStroke : String) : Pair<Boolean, List<CensusCertificate>> {
    if (
        !validateCertificateTimestamp(nextCert, mpt) ||
        !Encryptor.testPenStroke(penStroke, nextCert) ||
        !validateCertificate(nextCert)) {
      return Pair(false, listOf(nextCert))
    }

    val remainingData = dataSince(nextCert.timestamp)

    ledger =
        if (remainingData.isEmpty()) {
          ledger
        } else {
          ledger.subList(
              0,
              remainingData.get(0).index.toInt())
        }

    val datum =
        CensusDatum(
            ledger.last().index + 1,
            ledger.last().hash,
            nextCert,
            penStroke)

    if (!validateDatum(datum)) {
      ledger.addAll(remainingData)
      return Pair(false, listOf(nextCert))
    }

    ledger.add(datum)

    val rejectedCerts = mutableListOf<CensusCertificate>()
    for (d : CensusDatum in remainingData) {
      val addResult = addCertificate(d.cert, d.penStroke)
      if (!addResult.first) {
        rejectedCerts.addAll(addResult.second)
      }
    }
    return Pair(true, rejectedCerts)
  }

  fun comb(combs : List<Comb>) {
    val forward = false

    val iterator = ledger.listIterator(if (forward) 0 else ledger.size)
    var datum : CensusDatum

    while (if (forward) iterator.hasNext() else iterator.hasPrevious()) {
      datum = if (forward) iterator.next() else iterator.previous()
      for (comb in combs) {
        if (comb.applies(datum.cert)) {
          if (!comb.firstOnly || comb.hairs.size < 1) {
            comb.hairs.add(datum.cert)
          }
        }
      }
    }
  }

  fun dataSince(time : Long) : List<CensusDatum> {
    val firstIndex =
        ledger.indexOfFirst { d : CensusDatum ->
          d.cert.timestamp > time
        }

    return if (firstIndex < 0) listOf() else ledger.slice(
        IntRange(
            firstIndex,
            ledger.size - 1))
  }

  fun datum(num : Int) : CensusDatum {
    return ledger.get(num)
  }

  fun reject(networkRejectedCerts : List<CensusCertificate>) {
    val remainingData = dataSince(mpt).toMutableList()

    ledger = ledger.slice(
        IntRange(0, remainingData[0].index.toInt())).toMutableList()

    while(remainingData.isNotEmpty()) {
      if (remainingData[0].cert in networkRejectedCerts) {
        break
      }
      ledger.add(remainingData.removeAt(0))
    }

    for (datum in remainingData) {
      if (networkRejectedCerts.contains(datum.cert)) {
        continue
      }
      addCertificate(datum.cert, datum.penStroke)
    }
  }

  fun toByteArray() : ByteArray {
    return Json.encodeToString(ledger).toByteArray(Charsets.UTF_8)
  }

  override fun toString() : String {
    val builder = StringBuilder()
    for (datum in ledger) {
      builder.append(datum.toString() + "\n")
    }
    return builder.toString()
  }

  private fun validateCertificate(nextCert : CensusCertificate) : Boolean {
    val confereeMemberCertComb = Comb(
        listOf(
            ConfereeName(nextCert.conferee.name),
            Org(DEFAULTS.org),
            Role(DEFAULTS.memberRole)),
        true)
    val confereeRolesComb =
        MemberOrgRoleIsUnique(nextCert.conferee, nextCert.org)
    val issuerRolesComb =
        MemberOrgRoleIsUnique(nextCert.issuer, nextCert.org)
    val issuerIsGlobeIssuerComb = Comb(
        mutableListOf(
            Org(DEFAULTS.org),
            Conferee(nextCert.issuer),
            Role(DEFAULTS.issuerRole)),
        true)
    val orgRolesComb = OrgRoleIsUnique(nextCert.org)

    comb(
        listOf(
            confereeMemberCertComb,
            confereeRolesComb,
            issuerRolesComb,
            issuerIsGlobeIssuerComb,
            orgRolesComb))

    val confereeMemberCert =
        if (confereeMemberCertComb.hairs.isEmpty()) null
        else confereeMemberCertComb.hairs[0]
    val confereeExists =
        confereeMemberCert != null && !confereeMemberCert.revoke

    val issuerIsGlobeIssuer = (
        issuerIsGlobeIssuerComb.hairs.size > 0 &&
            !issuerIsGlobeIssuerComb.hairs[0].revoke)

    var issuerIsValid = false
    var confereeIsValid = false

    if (
        nextCert.org == DEFAULTS.org &&
        nextCert.role == DEFAULTS.memberRole) {
      issuerIsValid =
          issuerRolesComb.roles.contains(DEFAULTS.issuerRole) ||
              nextCert.issuer == nextCert.conferee
      confereeIsValid = (
          confereeMemberCert == null ||
              (confereeExists && nextCert.revoke) ||
              (
                  !confereeExists &&
                      !nextCert.revoke &&
                      confereeMemberCert.conferee != nextCert.conferee
                  ))
    } else if (
        nextCert.org == DEFAULTS.org &&
        nextCert.role == DEFAULTS.issuerRole) {
      issuerIsValid = issuerRolesComb.roles.contains(DEFAULTS.issuerRole)
      confereeIsValid =
          confereeExists &&
              !confereeRolesComb.roles.contains(DEFAULTS.issuerRole)
    } else if (nextCert.org == DEFAULTS.org) {
      return false
    } else if (orgRolesComb.roles.contains(nextCert.role)) {
      issuerIsValid = issuerRolesComb.roles.contains(DEFAULTS.issuerRole)
      confereeIsValid =
          confereeExists &&
              (confereeRolesComb.roles.contains(nextCert.role) ==
                  nextCert.revoke)
    } else if (
        orgRolesComb.roles.isNotEmpty() ||
        nextCert.role == DEFAULTS.issuerRole) {
      issuerIsValid = issuerIsGlobeIssuer
      confereeIsValid =
          confereeExists &&
              (confereeRolesComb.roles.contains(nextCert.role) == nextCert.revoke)
    }

    return issuerIsValid && confereeIsValid
  }

  private fun validateCertificateTimestamp(
      cert : CensusCertificate,
      mpt : Long) : Boolean {
    return (
        cert.timestamp > mpt &&
            cert.timestamp < System.currentTimeMillis() + maxFutureBlockTime)
  }

  private fun validateDatum(datum : CensusDatum) : Boolean {
    val correctIndex = datum.index == ledger.last().index + 1
    val correctHash = datum.previousHash == ledger.last().hash
    return correctIndex && correctHash
  }
}