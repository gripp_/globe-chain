package globe.census

import kotlinx.serialization.Serializable
import org.apache.commons.codec.digest.DigestUtils

@Serializable
class CensusDatum(
    val index : Long,
    val previousHash : String,
    val cert : CensusCertificate,
    val penStroke : String) {
  val hash = makeHash()

  override fun equals(other : Any?) : Boolean {
    if (other == null || other !is CensusDatum) {
      return false
    }
    val otherDatum : CensusDatum = other
    return (
        index == otherDatum.index &&
            previousHash == otherDatum.previousHash &&
            cert == otherDatum.cert &&
            hash == otherDatum.hash &&
            penStroke == otherDatum.penStroke)
  }

  override fun hashCode() : Int {
    val prime = 31
    var hashCode = 1
    hashCode = prime * hashCode + index.hashCode()
    hashCode = prime * hashCode + previousHash.hashCode()
    hashCode = prime * hashCode + cert.hashCode()
    hashCode = prime * hashCode + hash.hashCode()
    hashCode = prime * hashCode + penStroke.hashCode()
    return hashCode
  }

  override fun toString() : String {
    return "$index : $cert"
  }

  private fun makeHash() : String {
    val unhashedIdent : ByteArray = (
        (index.toString() + previousHash).toByteArray() +
            cert.toByteArray())
    return DigestUtils.sha256Hex(unhashedIdent)
  }
}