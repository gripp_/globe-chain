package globe.census

import java.security.InvalidParameterException
import java.util.regex.Pattern
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import kotlinx.serialization.encodeToString

@Serializable
class CensusCertificate(
  val org : String,
  val issuer : Member,
  val conferee : Member,
  val role : String,
  val timestamp : Long,
  val revoke : Boolean = false) {
  private val orgOrRoleRegex = "^([A-Z]{5,20})$"

  init {
    if (!Pattern.matches(orgOrRoleRegex, org)) {
      throw InvalidParameterException("Invalid org name.")
    }

    if (!Pattern.matches(orgOrRoleRegex, role)) {
      throw InvalidParameterException("Invalid role name.")
    }
  }

  override fun equals(other : Any?) : Boolean {
    if (other == null || other !is CensusCertificate) {
      return false
    }
    val otherCert : CensusCertificate = other
    return (
        org == otherCert.org &&
            issuer == otherCert.issuer &&
            conferee == otherCert.conferee &&
            role == otherCert.role &&
            revoke == otherCert.revoke)
  }

  override fun hashCode() : Int {
    val prime = 31
    var hashCode = 1
    hashCode = prime * hashCode + org.hashCode()
    hashCode = prime * hashCode + issuer.hashCode()
    hashCode = prime * hashCode + conferee.hashCode()
    hashCode = prime * hashCode + role.hashCode()
    hashCode = prime * hashCode + revoke.hashCode()
    return hashCode
  }

  override fun toString() : String {
    return (
        "org: $org / issuer: " + issuer.name + " / conferee: " + conferee.name +
            " / role: $role / timestamp: $timestamp / revoke: $revoke")
  }

  fun toByteArray() : ByteArray {
    return Json.encodeToString(this).toByteArray(Charsets.UTF_8)
  }
}