package globe.census.comb

import globe.census.CensusCertificate
import globe.census.comb.teeth.Tooth

open class Comb(val teeth : List<Tooth>, open val firstOnly : Boolean) {
  val hairs : MutableList<CensusCertificate> = mutableListOf()

  open fun applies(cert : CensusCertificate) : Boolean {
    var applies = true
    for (tooth in teeth) {
      applies = applies && tooth.applies(cert)
    }
    return applies
  }
}