package globe.census.comb.types

import globe.census.CensusCertificate
import globe.census.comb.Comb
import globe.census.comb.teeth.types.Org

class OrgRoleIsUnique(val org : String) : Comb(mutableListOf(Org(org)), false) {
  val roles = mutableSetOf<String>()

  override fun applies(cert : CensusCertificate) : Boolean {
    if (!super.applies(cert)) {
      return false
    }

    val seen = roles.contains(cert.role)
    roles.add(cert.role)
    return seen
  }
}