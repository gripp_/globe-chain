package globe.census.comb.types

import globe.census.CensusCertificate
import globe.census.comb.Comb

class OrgIsUnique : Comb(mutableListOf(), false) {
  val orgs = mutableSetOf<String>()

  override fun applies(cert : CensusCertificate) : Boolean {
    val seen = orgs.contains(cert.org)
    orgs.add(cert.org)
    return seen
  }
}