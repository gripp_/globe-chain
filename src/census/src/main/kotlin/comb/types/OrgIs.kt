package globe.census.comb.types

import globe.census.comb.Comb
import globe.census.comb.teeth.types.Org

class OrgIs(org : String, override val firstOnly : Boolean = false) :
    Comb(mutableListOf(Org(org)), firstOnly) {}