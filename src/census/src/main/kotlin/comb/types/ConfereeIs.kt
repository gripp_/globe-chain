package globe.census.comb.types

import globe.census.Member
import globe.census.comb.Comb
import globe.census.comb.teeth.types.Conferee

class ConfereeIs(
    conferee : Member, override val firstOnly : Boolean = false) :
    Comb(mutableListOf(Conferee(conferee)), firstOnly) {}