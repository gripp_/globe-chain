package globe.census.comb.types

import globe.census.Census
import globe.census.CensusCertificate
import globe.census.Member
import globe.census.comb.Comb
import globe.census.comb.teeth.types.Org
import globe.census.comb.teeth.types.Role

class EveryActiveMember() :
    Comb(listOf(
      Org(Census.DEFAULTS.org), Role(Census.DEFAULTS.memberRole)), false) {
  val memberNames = mutableSetOf<String>()
  val members = mutableSetOf<Member>()
  val revokedNames = mutableSetOf<String>()

  override fun applies(cert : CensusCertificate) : Boolean {
    if (!super.applies(cert)) {
      return false
    }

    if (
        memberNames.contains(cert.conferee.name) ||
        revokedNames.contains(cert.conferee.name)) {
      return false
    }

    if (cert.revoke) {
      revokedNames.add(cert.conferee.name)
    } else {
      memberNames.add(cert.conferee.name)
      members.add(cert.conferee)
    }

    return true
  }
}