package globe.census.comb.types

import globe.census.CensusCertificate
import globe.census.comb.Comb
import globe.census.comb.teeth.types.ConfereeName
import globe.census.comb.teeth.types.Role

class UniqueOrgWithMemberNameRole(name : String, role : String) :
    Comb(mutableListOf(ConfereeName(name), Role(role)), false) {
  val orgs = mutableSetOf<String>()
  val orgsRevoked = mutableSetOf<String>()

  override fun applies(cert : CensusCertificate) : Boolean {
    if (!super.applies(cert)) {
      return false
    }

    if (orgs.contains(cert.org) || orgsRevoked.contains(cert.org)) {
      return false
    }

    if (cert.revoke) orgsRevoked.add(cert.org) else orgs.add(cert.org)

    return true
  }
}