package globe.census.comb.types

import globe.census.comb.Comb
import globe.census.comb.teeth.types.Role

class RoleIs(role : String, override val firstOnly : Boolean = false) :
    Comb(mutableListOf(Role(role)), firstOnly) {}