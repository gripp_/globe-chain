package globe.census.comb.types

import globe.census.CensusCertificate
import globe.census.comb.Comb

class EveryRole() : Comb(listOf(), false) {
  val roles = mutableMapOf<String, MutableSet<String>>()

  override fun applies(cert : CensusCertificate) : Boolean {
    if (!roles.keys.contains(cert.org)) {
      roles[cert.org] = mutableSetOf()
    }

    val seen = roles[cert.org]!!.contains(cert.role)
    roles[cert.org]!!.add(cert.role)
    return seen
  }
}