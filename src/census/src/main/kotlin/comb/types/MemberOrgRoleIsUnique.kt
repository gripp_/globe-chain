package globe.census.comb.types

import globe.census.CensusCertificate
import globe.census.Member
import globe.census.comb.Comb
import globe.census.comb.teeth.types.Conferee
import globe.census.comb.teeth.types.Org

class MemberOrgRoleIsUnique(member : Member, val org : String) :
    Comb(mutableListOf(Org(org), Conferee(member)), false) {
  val roles = mutableSetOf<String>()
  val revoked = mutableSetOf<String>()
  val ungranted = mutableSetOf<String>()

  override fun applies(cert : CensusCertificate) : Boolean {
    val rolesContains = roles.contains(cert.role)
    val revokedContains = revoked.contains(cert.role)

    if (rolesContains || revokedContains) {
      return false
    }

    if (!super.applies(cert)) {
      ungranted.add(cert.role)
      return false
    }

    if (cert.revoke) revoked.add(cert.role) else roles.add(cert.role)
    ungranted.remove(cert.role)

    return true
  }
}