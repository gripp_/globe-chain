package globe.census.comb.teeth

import globe.census.CensusCertificate

interface Tooth {
  fun applies(cert : CensusCertificate) : Boolean
}