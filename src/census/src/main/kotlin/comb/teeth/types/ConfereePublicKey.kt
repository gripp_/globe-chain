package globe.census.comb.teeth.types

import globe.census.CensusCertificate
import globe.census.comb.teeth.Tooth

class ConfereePublicKey(val publicKey : String) : Tooth {
  override fun applies(cert : CensusCertificate) : Boolean {
    return cert.conferee.publicKey == publicKey
  }
}