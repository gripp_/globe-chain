package globe.census.comb.teeth.types

import globe.census.CensusCertificate
import globe.census.Member
import globe.census.comb.teeth.Tooth

class Conferee(val conferee : Member) : Tooth {
  override fun applies(cert : CensusCertificate) : Boolean {
    return cert.conferee == conferee
  }
}