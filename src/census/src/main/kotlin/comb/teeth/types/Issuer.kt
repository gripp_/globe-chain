package globe.census.comb.teeth.types

import globe.census.CensusCertificate
import globe.census.Member
import globe.census.comb.teeth.Tooth

class Issuer(val issuer : Member) : Tooth {
  override fun applies(cert : CensusCertificate) : Boolean {
    return cert.issuer == issuer
  }
}