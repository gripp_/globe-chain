package globe.census.comb.teeth.types

import globe.census.CensusCertificate
import globe.census.comb.teeth.Tooth

class ConfereeName(val name : String) : Tooth {
  override fun applies(cert : CensusCertificate) : Boolean {
    return cert.conferee.name == name
  }
}