package globe.census.comb.teeth.types

import globe.census.CensusCertificate
import globe.census.comb.teeth.Tooth

class Org(val org : String) : Tooth {
  override fun applies(cert : CensusCertificate) : Boolean {
    return cert.org == org
  }
}