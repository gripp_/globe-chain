package globe.census

import java.security.SecureRandom
import java.util.*
import java.util.regex.Pattern
import javax.crypto.Cipher
import javax.crypto.SecretKey
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.PBEKeySpec
import javax.crypto.spec.SecretKeySpec

object Encryptor {
  private const val cipherAlgorithm = "AES/CBC/PKCS5Padding"
  private const val iterationCount = 5000
  private const val ivLength = 16
  private const val keyAlgorithm = "PBKDF2WithHmacSHA1"
  private const val keyLength = 256
  private const val saltLength = 8

  private val decoder = Base64.getDecoder()
  private val encoder = Base64.getEncoder()

  const val everyValidSignatureCharacter = (
      "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ~@#\$%^&" +
          "*+|?/.,")
  private const val validNameRegex = "^([a-z0-9]{5,20})$"
  private const val validSignatureRegex = (
      "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[~@#$%^&*+|?/.,]).{32,32}$")

  fun decrypt(
    message : String, name : String, signature : String, salt : String) :
      String {
    return decryptMessage(
      message, makePrivateKey(name, signature, salt), name)
  }

  fun encrypt(message : String, member : Member) : String {
    val key = makePublicKey(member.publicKey)
    return encryptMessage(message, key, member.name)
  }

  fun makePublicKeyString(
    name : String, signature : String, salt : String) : String {
    val key = makePBEKey(name, signature, salt)
    return encoder.encodeToString(key.encoded)
  }

  fun makeSalt() : String {
    val salt = ByteArray(saltLength)
    SecureRandom().nextBytes(salt)
    return encoder.encodeToString(salt)
  }

  fun makeRandomSignature() : String {
    val signatureBuilder = StringBuilder()
    while (!validateSignature(signatureBuilder.toString())) {
      signatureBuilder.clear()

      for (i in 0 until 32) {
        signatureBuilder.append(
          everyValidSignatureCharacter[
              SecureRandom().nextInt(everyValidSignatureCharacter.length)])
      }
    }
    return signatureBuilder.toString()
  }

  fun makePenStroke(
    cert : CensusCertificate,
    member : Member,
    signature : String) : String {

    return encryptMessage(
      cert.toByteArray().toString(Charsets.UTF_8),
      makePrivateKey(member.name, signature, member.salt),
      member.name)
  }

  fun testPenStroke(
    penStroke : String, cert : CensusCertificate) : Boolean {

    return decryptMessage(
      penStroke,
      makePublicKey(cert.issuer.publicKey),
      cert.issuer.name) == cert.toByteArray().toString(Charsets.UTF_8)
  }

  fun validateName(name : String) : Boolean {
    return Pattern.matches(validNameRegex, name)
  }

  fun validateSignature(signature : String) : Boolean {
    if (signature.length != 32) {
      return false
    }

    for (character in signature) {
      if (!everyValidSignatureCharacter.contains(character)) {
        return false
      }
    }
    return Pattern.matches(validSignatureRegex, signature)
  }

  private fun decryptMessage(
    message : String, key : SecretKey, name : String) : String {
    val cipher = Cipher.getInstance(cipherAlgorithm)
    cipher.init(Cipher.DECRYPT_MODE, key, makeIvParameterSpec(name))
    return String(cipher.doFinal(decoder.decode(message)))
  }

  private fun encryptMessage(
    message : String, key : SecretKey, name : String) : String {
    val cipher = Cipher.getInstance(cipherAlgorithm)
    cipher.init(Cipher.ENCRYPT_MODE, key, makeIvParameterSpec(name))
    val encrypted = cipher.doFinal(message.toByteArray(Charsets.UTF_8))
    return String(encoder.encode(encrypted))
  }

  private fun makePublicKey(publicKey : String) : SecretKey {
    val keyBytes = decoder.decode(publicKey)
    return SecretKeySpec(keyBytes, "AES")
  }

  private fun makePrivateKey(
    name : String, signature : String, salt : String) : SecretKey {
    val key = makePBEKey(name, signature, salt)
    return SecretKeySpec(key.encoded, "AES")
  }

  private fun makeIvParameterSpec(name : String) : IvParameterSpec {
    val builder = StringBuilder()
    builder.append(name)
    builder.append("*".repeat(ivLength))
    return IvParameterSpec(
      builder.toString().toByteArray(Charsets.UTF_8), 0, ivLength)
  }

  private fun makePBEKey(
    name : String, signature : String, salt : String) : SecretKey {
    val keySpec = PBEKeySpec(
      (name + signature).toCharArray(),
      decoder.decode(salt),
      iterationCount,
      keyLength)
    return SecretKeyFactory.getInstance(keyAlgorithm).generateSecret(keySpec)
  }
}