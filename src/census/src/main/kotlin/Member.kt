package globe.census

import kotlinx.serialization.Serializable

@Serializable
class Member(val name : String, val publicKey : String, val salt : String) {
  override fun equals(other : Any?) : Boolean {
    if (other == null || other !is Member) {
      return false
    }
    val otherMember : Member = other
    return (
        name == otherMember.name &&
            publicKey == otherMember.publicKey &&
            salt == otherMember.salt)
  }

  override fun hashCode() : Int {
    val prime = 31
    var hashCode = 1
    hashCode = prime * hashCode + name.hashCode()
    hashCode = prime * hashCode + publicKey.hashCode()
    hashCode = prime * hashCode + salt.hashCode()
    return hashCode
  }
}