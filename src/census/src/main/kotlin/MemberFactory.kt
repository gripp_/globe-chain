package globe.census

import java.lang.StringBuilder
import java.security.InvalidParameterException

object MemberFactory {
  fun make(name : String) : Pair<Member, String> {
    if (!Encryptor.validateName(name)) {
      throw InvalidParameterException("Invalid characters in name.")
    } else if (name != name.toLowerCase()) {
      throw InvalidParameterException(
        "Names may contain only lowercase letters.")
    }

    val signature = Encryptor.makeRandomSignature()

    val salt = Encryptor.makeSalt()
    val publicKey = Encryptor.makePublicKeyString(name, signature, salt)
    return Pair(Member(name, publicKey, salt), signature)
  }
}