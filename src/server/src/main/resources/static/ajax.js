let rpc_confer = (conferee, role, org, issuer, signature, callback) => {
  _call(
      "confer",
      {
        "conferee": conferee,
        "issuer": issuer,
        "org": org,
        "role": role,
        "issuerSignature": signature,
      },
      callback);
};

let rpc_create = (name, callback) => {
  _call("create", { "name": name }, callback);
};

let rpc_get_privileges = (name, callback) => {
  _call("get_privileges", { "name": name }, callback);
};

let _call = (path, data, callback) => {
  $.ajax({
    url: "/api/" + path,
    type: "POST",
    data: JSON.stringify(data),
    contentType: "application/json",
    dataType: "json",
    success: callback
  });
};