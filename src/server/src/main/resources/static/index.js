var activeMemberName = undefined;
var allMembers = undefined;
var orgsToRoles = undefined;

var ELEMENTS = {
  // BUTTONS
  conferButton: "button#confer",
  createButton: "button#create",
  createMemberButton: "button#create-member",
  getPrivilegesButton: "button#get-privileges",
  startConferButton: "button#start-confer",

  // DATASETS
  membersDatalist: "datalist#members",
  orgsDatalist: "datalist#orgs",
  rolesDatalist: "datalist#roles",

  // INPUT ELEMENTS
  activeMemberNameInput: "input#active-member-name",
  newMemberNameInput: "input#new-member-name",
  memberInput: "input#member",
  orgInput: "input#org",
  roleInput: "input#role",
  signatureInput: "input#signature",

  // INPUT SETS
  conferRoleOptions: "div#confer-role-options",
  createMemberOptions: "span#create-member-options",
  lookupPrivilegesOptions: "span#lookup-privileges-options",
  nextActionOptions: "span#next-action-options",

  // SECTIONS
  input: "form#inputs",
  output: "span#output",
};

var RESPONSE_KEYS = {
  members: "members",
  name: "name",
  roles: "roles",
  signature: "signature",
  success: "success",
};
var SUCCESS = "success";

let click_confer = (e) => {
  rpc_confer(
      ELEMENTS.memberInput.val(),
      ELEMENTS.roleInput.val(),
      ELEMENTS.orgInput.val(),
      activeMemberName,
      ELEMENTS.signatureInput.val(),
      receive_confer);
};

let click_create = (e) => {
  let name = ELEMENTS.newMemberNameInput.val();
  rpc_create(name, receive_create);
};

let click_createMember = (e) => {
  ELEMENTS.nextActionOptions.hide();
  ELEMENTS.createMemberOptions.show();
};

let click_getPrivileges = (e) => {
  activeMemberName = ELEMENTS.activeMemberNameInput.val();
  ELEMENTS.signatureInput.attr(
      "placeholder",
      "Signature for " + activeMemberName + "...");
  rpc_get_privileges(activeMemberName, receive_privileges);
};

let click_startConfer = (e) => {
  ELEMENTS.nextActionOptions.hide();
  ELEMENTS.lookupPrivilegesOptions.show();
};

let receive_confer = (response) => {
  ELEMENTS.lookupPrivilegesOptions.hide();
  ELEMENTS.conferRoleOptions.show();

  if (!response[RESPONSE_KEYS.success])  {
    return;
  }

  ELEMENTS.output.text("Confer successful!");
  ELEMENTS.output.show();
  ELEMENTS.input.hide();
};

let receive_create = (response) => {
  if (!response[RESPONSE_KEYS.success])  {
    return;
  }

  ELEMENTS.output.text(
      "Create successful! Note signature " +
      response[RESPONSE_KEYS.signature] +
      " for member " +
      response[RESPONSE_KEYS.name]);
  ELEMENTS.output.show();
  ELEMENTS.input.hide();
};

let receive_privileges = (response) => {
  if (!response[RESPONSE_KEYS.success])  {
    return;
  }

  ELEMENTS.lookupPrivilegesOptions.hide();

  allMembers = response[RESPONSE_KEYS.members];
  orgsToRoles = response[RESPONSE_KEYS.roles];

  ELEMENTS.orgsDatalist.empty();
  Object.keys(orgsToRoles).forEach(
      (org) => {
        let option = $("<option></option>");
        option.text(org);
        option.val(org);
        ELEMENTS.orgsDatalist.append(option);
      });

  updateRoles();

  ELEMENTS.membersDatalist.empty();
  allMembers.forEach(
      (member) => {
        let option = $("<option></option>");
        option.text(member);
        option.val(member);
        ELEMENTS.membersDatalist.append(option);
      });

  ELEMENTS.conferRoleOptions.show();
};

let init = () => {
  $("button").click((e) => { e.preventDefault(); });

  Object.keys(ELEMENTS).forEach(
    (key) => {
      ELEMENTS[key] = $(ELEMENTS[key]);
    });

  ELEMENTS.conferButton.click(click_confer);
  ELEMENTS.createButton.click(click_create);
  ELEMENTS.createMemberButton.click(click_createMember);
  ELEMENTS.getPrivilegesButton.click(click_getPrivileges);
  ELEMENTS.startConferButton.click(click_startConfer);
  ELEMENTS.orgInput.change(updateRoles);
};

let updateRoles = () => {
  ELEMENTS.rolesDatalist.empty();

  if (!orgsToRoles || !(ELEMENTS.orgInput.val() in orgsToRoles)) {
    return;
  }

  orgsToRoles[ELEMENTS.orgInput.val()].forEach(
      (role) => {
        let option = $("<option></option>");
        option.text(role);
        option.val(role);
        ELEMENTS.rolesDatalist.append(option);
      });
};

$(document).ready(init);