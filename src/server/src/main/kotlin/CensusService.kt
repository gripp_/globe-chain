package globe.server

import globe.census.*
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import org.springframework.stereotype.Service
import org.springframework.util.ResourceUtils
import java.io.File

@Service
class CensusService(
    private val properties : ServerProperties, network : NetworkService) {
  val census : Census

  init {
    val file = ResourceUtils.getFile(properties.censusFile)

    val seedLedger : List<CensusDatum> = if (file.exists()) {
      Json.decodeFromString(file.readLines().joinToString(""))
    } else {
      network.seedLedger()
    }

    census = if (seedLedger.isNotEmpty()) {
      Census(seedLedger)
    } else {
      println("enter first issuer name?")
      val name = readLine()!!

      val firstMemberInfo = MemberFactory.make(name)
      val firstMember = firstMemberInfo.first
      val newCensus = Census(firstMember, firstMemberInfo.second)

      println(
          "census initialized. first member is $name with signature " +
              firstMemberInfo.second)

      newCensus
    }

    save()
  }

  fun save() {
    File(properties.censusFile).outputStream().write(census.toByteArray())
  }
}