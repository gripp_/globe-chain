package globe.server

import globe.census.Census
import globe.census.CensusCertificate
import globe.census.CensusDatum
import java.io.File
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.springframework.stereotype.Service
import org.springframework.util.ResourceUtils
import java.io.IOException
import java.lang.NumberFormatException
import java.net.*
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse
import java.util.*

@Service
class NetworkService(private val properties : ServerProperties) {
  private val peers : MutableSet<InetAddress>

  init {
    val file = ResourceUtils.getFile(properties.peersFile)

    peers = if (file.exists()) {
      val addresses = Json.decodeFromString<List<String>>(
          file.readLines().joinToString("")
      ).toSet()
      (addresses.map { a -> InetAddress.getByName(a) }).toMutableSet()
    } else {
      mutableSetOf()
    }
  }

  fun exchangeLedger(census : Census) {
    peers.remove(InetAddress.getByName("localhost"))

    var peer : InetAddress
    var attempts = 0

    while (peers.isNotEmpty() && attempts < properties.peerConnectionAttempts) {
      peer = peers.shuffled()[0]

      try {
        exchangeLedgerWith(census, Socket(peer, properties.consensusPort))
        break
      } catch (e : IOException) {
        peers.remove(peer)
        attempts++
        continue
      }
    }

    exchangeLedgerWith(census, ServerSocket(properties.consensusPort).accept())
    save()
  }

  fun seedLedger() : List<CensusDatum> {
    for (peer in peers) {
      try {
        return Json.decodeFromString(
            URL(
                "http://".plus(peer.hostAddress)
                    .plus(":8081/api/ledger")).readText(Charsets.UTF_8))
      } catch (e : IOException) {
        continue
      }
    }

    return listOf()
  }

  private fun exchangeLedgerWith(census : Census, socket : Socket) {
    val reader = socket.getInputStream().bufferedReader(Charsets.UTF_8)
    val writer = socket.getOutputStream()

    val rejectedCerts = mutableListOf<CensusCertificate>()

    if (!socket.isConnected) {
      return
    }

    // Send list of peers
    writer.write(
        Json.encodeToString<List<ByteArray>>(peers.map { p -> p.address })
            .plus("\n").toByteArray())

    // Send current MPT.
    writer.write(
        census.mpt.toString().plus("\n").toByteArray(Charsets.UTF_8))

    // Add new peers.
    peers.addAll(
        Json.decodeFromString<List<ByteArray>>(reader.readLine())
            .map { a -> InetAddress.getByAddress(a) })

    // Read network MPT.
    val networkMpt = reader.readLine().toLong()

    // Send recent data.
    writer.write(
        Json.encodeToString(census.dataSince(networkMpt)).plus("\n")
            .toByteArray())

    // Read and process network ledger.
    val networkLedger =
        Json.decodeFromString<List<CensusDatum>>(reader.readLine())
            .filter { datum -> datum.cert.timestamp > census.mpt }

    for (datum : CensusDatum in networkLedger) {
      val response = census.addCertificate(datum.cert, datum.penStroke)
      if (!response.first) {
        rejectedCerts.addAll(response.second)
      }
    }

    // Send rejected certs.
    writer.write(Json.encodeToString(rejectedCerts).plus("\n").toByteArray())

    // Receive and process network rejected certs.
    val networkRejectedCerts =
        Json.decodeFromString<List<CensusCertificate>>(reader.readLine())
    census.reject(networkRejectedCerts)
  }

  private fun save() {
    File(properties.peersFile).outputStream().write(
        Json.encodeToString(peers.map { p -> p.hostAddress }).plus("\n")
            .toByteArray(Charsets.UTF_8))
  }
}