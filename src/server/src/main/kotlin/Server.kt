package globe.server

import com.fasterxml.jackson.annotation.JsonProperty
import globe.census.*
import globe.census.comb.Comb
import globe.census.comb.teeth.types.ConfereeName
import globe.census.comb.teeth.types.Org
import globe.census.comb.teeth.types.Role
import globe.census.comb.types.EveryActiveMember
import globe.census.comb.types.EveryRole
import globe.census.comb.types.UniqueOrgWithMemberNameRole
import org.springframework.http.MediaType
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.web.bind.annotation.*

@RestController
class Server(
    private val censusService : CensusService,
    private val networkService : NetworkService) {

  data class ConferRequest(
      @JsonProperty("conferee") val conferee : String,
      @JsonProperty("issuer") val issuer : String,
      @JsonProperty("org") val org : String,
      @JsonProperty("role") val role : String,
      @JsonProperty("issuerSignature") val issuerSignature : String)

  data class Name constructor(@JsonProperty("name") val name : String)

  data class Privileges(
      val success : Boolean,
      val roles : Map<String, Set<String>>,
      val members : List<String>)

  data class Signature constructor(
      val success : Boolean, val name : String, val signature : String)

  data class Success constructor(val success : Boolean)

  @PostMapping(
      path = ["/api/confer"],
      consumes = [MediaType.APPLICATION_JSON_VALUE],
      produces = [MediaType.APPLICATION_JSON_VALUE])
  fun confer(@RequestBody request : ConferRequest) : Success {
    val census = censusService.census

    val conferee = request.conferee.toLowerCase()
    val issuer = request.issuer.toLowerCase()
    val org = request.org.toUpperCase()
    val role = request.role.toUpperCase()

    val confereeMemberCertComb = Comb(
        listOf(
            ConfereeName(conferee),
            Org(Census.DEFAULTS.org),
            Role(Census.DEFAULTS.memberRole)),
        true)
    val issuerMemberCertComb = Comb(
        listOf(
            ConfereeName(issuer),
            Org(Census.DEFAULTS.org),
            Role(Census.DEFAULTS.memberRole)),
        true)

    census.comb(listOf(confereeMemberCertComb, issuerMemberCertComb))

    if (
        confereeMemberCertComb.hairs.isEmpty() ||
        issuerMemberCertComb.hairs.isEmpty()) {
      return Success(false)
    }

    val confereeMember = confereeMemberCertComb.hairs[0].conferee
    val issuerMember = issuerMemberCertComb.hairs[0].conferee
    val cert = CensusCertificate(
        org,
        issuerMember,
        confereeMember,
        role,
        System.currentTimeMillis())
    return Success(
        census.addCertificate(
            cert,
            Encryptor.makePenStroke(
                cert,
                issuerMember,
                request.issuerSignature)).first)
  }

  @PostMapping(
      path = ["/api/create"],
      consumes = [MediaType.APPLICATION_JSON_VALUE],
      produces = [MediaType.APPLICATION_JSON_VALUE])
  fun create(@RequestBody request : Name) : Signature {
    val newConfereeInfo = MemberFactory.make(request.name)
    val cert = CensusCertificate(
        Census.DEFAULTS.org,
        newConfereeInfo.first,
        newConfereeInfo.first,
        Census.DEFAULTS.memberRole,
        System.currentTimeMillis())
    val success = censusService.census.addCertificate(
        cert,
        Encryptor.makePenStroke(
            cert,
            newConfereeInfo.first,
            newConfereeInfo.second))

    return Signature(
        success.first,
        if (success.first) newConfereeInfo.first.name else "",
        if (success.first) newConfereeInfo.second else "")
  }

  @PostMapping(
      path = ["/api/get_privileges"],
      consumes = [MediaType.APPLICATION_JSON_VALUE],
      produces = [MediaType.APPLICATION_JSON_VALUE])
  fun getPrivileges(@RequestBody request : Name) : Privileges {
    val census = censusService.census
    val memberCertComb = Comb(
        listOf(
            ConfereeName(request.name),
            Org(Census.DEFAULTS.org),
            Role(Census.DEFAULTS.memberRole)),
        true)
    val issuerOrgsComb =
        UniqueOrgWithMemberNameRole(request.name, Census.DEFAULTS.issuerRole)
    val rolesComb = EveryRole()
    val membersComb = EveryActiveMember()
    census.comb(listOf(memberCertComb, issuerOrgsComb, rolesComb, membersComb))

    if (memberCertComb.hairs.isEmpty() || memberCertComb.hairs[0].revoke) {
      return Privileges(false, mapOf(), listOf())
    }

    return Privileges(true, rolesComb.roles, membersComb.memberNames.toList())
  }

  @GetMapping(
      path = ["/api/ledger"],
      produces = [MediaType.APPLICATION_JSON_VALUE])
  fun ledger() : List<CensusDatum> {
    return censusService.census.snapshot
  }

  @Scheduled(fixedRate = 60000)
  fun exchange() {
    networkService.exchangeLedger(censusService.census)
    censusService.save()
  }
}