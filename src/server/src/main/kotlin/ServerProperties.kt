package globe.server

import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.PropertySource
import org.springframework.stereotype.Component

@Component
@ConfigurationProperties
@PropertySource("classpath:server.properties")
data class ServerProperties(
    @Value("\${school.construct.globe.server.censusFile}")
    var censusFile : String,

    @Value("\${school.construct.globe.server.consensusPort}")
    var consensusPort : Int,

    @Value("\${school.construct.globe.server.peerConnectionAttempts}")
    var peerConnectionAttempts : Int,

    @Value("\${school.construct.globe.server.peerConnectionTimeout}")
    var peerConnectionTimeout : Int,

    @Value("\${school.construct.globe.server.peersFile}")
    var peersFile : String)