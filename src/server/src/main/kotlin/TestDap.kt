package globe.server

import globe.census.CensusCertificate
import globe.census.CensusDatum
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.IOException
import java.net.InetAddress
import java.net.Socket

fun main() {
  val peer = InetAddress.getByName("localhost")

  try {
    val socket = Socket(peer, 49149)
    val reader = socket.getInputStream().bufferedReader(Charsets.UTF_8)
    val writer = socket.getOutputStream()

    var networkMpt : Long = -1
    val rejectedCerts = mutableListOf<CensusCertificate>()

    if (!socket.isConnected) {
      return
    }

    // Send empty list of peers.
    writer.write(
        Json.encodeToString<List<ByteArray>>(listOf()).plus("\n").toByteArray())

    // Send fake MPT.
    writer.write(0.toString().plus("\n").toByteArray(Charsets.UTF_8))

    // Read peers.
    val peers = reader.readLine()

    // Read network MPT.
    networkMpt = reader.readLine().toLong()

    // Send empty recent data.
    writer.write(
        Json.encodeToString<List<CensusDatum>>(listOf()).plus("\n")
            .toByteArray())

    // Read network ledger.
    val networkLedger =
        Json.decodeFromString<List<CensusDatum>>(reader.readLine())

    // Write empty rejected certs.
    writer.write(Json.encodeToString(rejectedCerts).plus("\n").toByteArray())

    // Read network rejected certs.
    val networkRejectedCerts =
        Json.decodeFromString<List<CensusCertificate>>(reader.readLine())
  } catch (e : IOException) {
    // Damn!
    println("Damn!")
  }
}