plugins {
  java
  kotlin("jvm") version "1.4.10"
  id("application")
  id("com.github.johnrengelman.shadow") version "5.2.0"
}

group = "school.construct"
version = "0.1-ALPHA"

repositories {
  mavenCentral()
}

dependencies {
  implementation(kotlin("stdlib"))
  implementation("commons-codec:commons-codec:1.15")

  implementation(project(":server"))

  testImplementation("junit", "junit", "4.12")
}

application {
  mainClassName = "globe.server.RunServerKt"
}